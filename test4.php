<?php

function flattenArray(array $array) {
    $return 	= array();
    array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; }); // using closure
    return $return;
}
$compress 		= flattenArray(array(1,2,array(3,4, array(5,6,7), 8), 9));
